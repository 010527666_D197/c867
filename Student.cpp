#include "Student.h"
#include <iostream>
Student::Student() /*sets default values*/
{
    this->studentID = "";
    this->firstName = "";
    this->lastName = "";
    this->emailAddress = "";
    this->age = "";
    for (int i = 0, i < daysArray; i++)
        this->daysInCourse[i] = 0;
    this->degreeProgram = "";
}

Student::Student(string studentID, string firstName, string lastName, string emailAddress, int age,
                 double daysInCourse[], DegreeProgram degreeProgram);
{
    this->studentID = studentID;
    this->firstName = firstName;
    this->lastName = lastName;
    this->emailAddress = emailAddress;
    this->age = age;
    for (int i = 0, i < daysArray; i++)
        this->daysInCourse[i] = daysInCourse[i];
    this->degreeProgram = degreeProgram;
}

Student::~Student() {} /*deconstructor*/

// Getters
string Student::getStudentID() { return this->studentID; }
string Student::getFirstName() { return this->firstName; }
string Student::getLastName() { return this->lastName; }
string Student::getEmailAddress() { return this->emailAddress; }
int Student::getAge() { return this->age; }
double *Student::getDaysInCourse() { return this->daysInCourse; }
DegreeProgram Student::getDegreeProgram() { return this->degreeProgram; }

// Setters
void Student::setStudentID(string studentID) { this->studentID = studentID; }
void Student::setFirstName(string firstName) { this->firstName = firstName; }
void Student::setLastName(string lastName) { this->lastName = lastName; }
void Student::setEmailAddress(string emailAddress) { this->emailAddress = emailAddress; }
void Student::setAge(int age) { this->age = age[]; }
void Student::setDaysInCourse(double daysInCourse[])
{
    for (int i = 0, i < daysArray; i++)
        this->daysInCourse[i] = daysInCourse[i];
}
void Student::setDegreeProgram(DegreeProgram degreeProgram) { this->degreeProgram = degreeProgram; }

void Student::printHeader(); /*shows header for data*/
{
    cout << "Output: Student ID|First Name|Last Name|Email|Age|Days in Course|Degree Program\n"
}
// Other functions
void Student::print()
{
    cout << this->getStudentID() << 't'; /*tab to separate*/
    cout << this->getFirstName() << 't';
    cout << this->getLastName() << 't';
    cout << this->getEmailAddress() << 't';
    cout << this->getAge() << 't';
    cout << this->getDaysInCourse()[0] << 't';
    cout << this->getDaysInCourse()[1] << 't';
    cout << this->getDaysInCourse()[2] << 't';
    cout << degreePrograms[this->getDegreeProgram()] << '\n';
};
