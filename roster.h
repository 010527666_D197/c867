#pragma once
#include "Student.h"
#include <iostream>

class Roster
{
public:
    int lastIndex = -1;
    const static int numStudents = 5;       // Index to keep track of the last filled position in the roster
    Student *classRosterArray[numStudents]; // Assuming 5 students in the roster

public:
    // Function to add a student
    void parse(string row);
    void add(string studentID, string firstName, string lastName, string emailAddress, int age,
             double daysInCourse1, double daysInCourse2, double daysInCourse3, DegreeProgram degreeProgram);

    // Function to print all student data
    void printAll();

    // Function to print students by degree program
    void printByDegreeProgram(DegreeProgram degreeProgram);

    // Function to print invalid email addresses
    void printInvalidEmails();

    // Function to print average days in course for a student
    void printAverageDaysInCourse(string studentID);

    // Function to remove a student by ID
    bool remove(string studentID);

    // Destructor declaration with noexcept
    ~Roster();
};
