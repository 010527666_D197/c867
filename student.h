#pragma once
#include <iomanip>
#include <iostream>
#include "Degree.h"
using std::cout;
using std::string;

class Student
{
public:
    const static int daysArray = 3;

private:
    string studentID;
    string firstName;
    string lastName;
    string emailAddress;
    int age;
    double daysInCourse[daysArray];
    DegreeProgram degreeProgram;

public:
    Student(); /*Paramater less Constructor*/
    Student(string studentID, string firstName, string lastName, string emailAddress, int age,
            double daysInCourse[], DegreeProgram degreeProgram); /*Full Constructor*/
    ~Student(); /*deconstructor*/                                /*Deconstructor*/

    /* Getters*/
    string getStudentID();
    string getFirstName();
    string getLastName();
    string getEmailAddress();
    int getAge();
    double *getDaysInCourse();
    DegreeProgram getDegreeProgram();

    /* Setters*/
    void setStudentID(string studentID);
    void setFirstName(string firstName);
    void setLastName(string lastName);
    void setEmailAddress(string emailAddress);
    void setAge(int age);
    void setDaysInCourse(double daysInCourse[]);
    void setDegreeProgram(DegreeProgram degreeProgram);

    static void printHeader(); /*shows header for data*/
    // Other functions
    void print(); /*shows student's information*/
};
