#include "Roster.h"
#include "Student.h"
#include <iostream>
#include <iomanip>
#include <string>

void Roster::parse(string studentData)
{
    DegreeProgram degreeProgram = NETWORK; // network default
    if (studentData.at(1) == '1')
        degreeProgram = SECURITY;
    else if (studentData.at(1) == '4')
        degreeProgram = SECURITY;
    else if (studentData.at(1) == '3')
        degreeProgram = SOFTWARE;
    else if (studentData.at(1) == '5')
        degreeProgram = SOFTWARE;

    int soc = studentData.find(",");         /*finding comma*/
    string sID = studentData.substr(0, soc); /*gets substring in front of comma*/

    int osoc = soc + 1; /*moving past comma*/
    soc = studentData.find(",", osoc);
    string sFN = studentData.substr(osoc, soc - osoc); /*first name*/

    osoc = soc + 1; /*moving forward*/
    soc = studentData.find(",", osoc);
    string sLN = studentData.substr(osoc, soc - osoc); /*last name*/

    osoc = soc + 1; /*moving forward*/
    soc = studentData.find(",", osoc);
    string sEA = studentData.substr(osoc, soc - osoc); /*email*/

    osoc = soc + 1; /*moving forward*/
    soc = studentData.find(",", osoc);
    int sAge = stod(studentData.substr(osoc, soc - osoc)); /*age*/

    osoc = soc + 1; /*moving forward*/
    soc = studentData.find(",", osoc);
    double days1 = stod(studentData.substr(osoc, soc - osoc)); /*days in course 1*/
    osoc = soc + 1;
    soc = studentData.find(",", osoc);
    double days2 = stod(studentData.substr(osoc, soc - osoc)); /*days in course 2*/
    osoc = soc + 1;
    soc = studentData.find(",", osoc);
    double days3 = stod(studentData.substr(osoc, soc - osoc)); /*days in course 3*/

    add(sID, sFN, sLN, sEA, sAge, days1, days2, days3, degreeProgram);
}
void Roster::add(string studentID, string firstName, string lastName, string emailAddress, int age,
                 double daysInCourse1, double daysInCourse2, double daysInCourse3, DegreeProgram degreeProgram)
{
    /*putting days in courses in array*/
    double days[3] = {daysInCourse1, daysInCourse2, daysInCourse3};

    classRosterArray[++lastIndex] = new Student(studentID, firstName, lastName, emailAddress, age,
                                                days, degreeProgram); /*full constructor*/
}

/*display*/
void Roster::printAll()
{
    for (int i = 0; i <= Roster::lastIndex; i++)
    {
        if (classRosterArray[i] != nullptr)
        {
            classRosterArray[i]->print();
        }
    }
}

/*matching degree programs*/
void Roster::printByDegreeProgram(DegreeProgram degreeProgram)
{
    for (int i = 0; i <= Roster::lastIndex; i++)
    {
        if (Roster::classRosterArray[i]->getDegreeProgram() == degreeProgram)
            classRosterArray[i]->print();
    }
    cout << std::endl;
}

/*Valid email addresses should include "@", ".", and not include a space " "*/
void Roster::printInvalidEmails()
{
    bool any = false;
    for (int i = 0; i <= Roster::lastIndex; i++)
    {
        string sEA = (classRosterArray[i]->getEmailAddress());
        if (sEA.find(' ') != string::npos || (sEA.find('@') == string::npos || sEA.find('.') == string::npos))
        {
            any = true;
            cout << sEA << ":" << classRosterArray[i]->getStudentID() << std::endl;
        }
    }
    if (!any)
        cout << "NO VALID EMAILS" << std::endl;
}

void Roster::printAverageDaysInCourse(string studentID)
{
    for (int i = 0; i <= Roster::lastIndex; i++)
    {
        if (Roster::classRosterArray[i]->getStudentID() == studentID)
        {
            cout << studentID << ": ";
            cout << static_cast<double>((classRosterArray[i]->getDaysInCourse()[0]) +
                                        classRosterArray[i]->getDaysInCourse()[1] +
                                        classRosterArray[i]->getDaysInCourse()[2]) /
                        3
                 << std::endl;
        }
    }
    cout << std::endl;
}

bool Roster::remove(string studentID) /* student ID removal*/
{
    bool success = false;
    for (int i = 0; i <= Roster::lastIndex; i++)
    {
        if (classRosterArray[i] == nullptr)
        {
            continue;
        }
        if (classRosterArray[i]->getStudentID() == studentID)
        {
            classRosterArray[i] = nullptr;
            success = true; /* it is found*/
        }
    }
    if (success)
    {
        cout << studentID << "Removed" << std::endl
             << std::endl;
    }
    else
    {
        cout << studentID << " Not Removed." << std::endl
             << std::endl;
    }
}

Roster::~Roster() // Deconstructor
{
    for (int i = 0; i <= Roster::lastIndex; i++)
    {
        if (classRosterArray[i] != nullptr)
        {
            cout << "Destructor call for student" << classRosterArray[i]->getStudentID() << std::endl;
            delete classRosterArray[i];
            classRosterArray[i] = nullptr;
        }
    }
}