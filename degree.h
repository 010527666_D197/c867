#pragma once
#include <string>

enum DegreeProgram
{
    SECURITY,
    NETWORK,
    SOFTWARE
};

static const std::string degreeProgram[] = {"SECURITY", "NETWORK", "SOFTWARE"};