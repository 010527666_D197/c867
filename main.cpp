#include "Roster.h" //Here it is//
#include "Roster.cpp"
#include "Student.h"

#include <iostream>

int main()
{
    cout << "Scripting and Programming - Applications - C867, C++, 010527666, Wako Adem" << std::endl;
    const string studentData[] =
        {
            "A1, John, Smith, John1989@gm ail.com, 20, 30, 35, 40, SECURITY",
            "A2, Suzan, Erickson, Erickson_1990@gmailcom, 19, 50, 30, 40, NETWORK",
            "A3, Jack, Napoli, The_lawyer99yahoo.com, 19, 20, 40, 33, SOFTWARE",
            "A4, Erin, Black, Erin.black@comcast.net, 22, 50, 58, 40, SECURITY",
            "A5, Walter, Adam, Wadam@wgu.edu, 26, 50, 30 , 40, SOFTWARE"};
    const int numStudents = 5; /*Creates roster- default*/
    Roster classRoster;

    for (int i = 0; i < numStudents; i++)
        classRoster.parse(studentData[i]);
    cout << "Show all Students: " << std::endl;
    classRoster.printAll();
    cout << std::endl;

    for (int i = 0; i < 3; i++)
    {
        cout << "Show degree program: " << degreeProgram[i] << std::endl;
        classRoster.printByDegreeProgram((DegreeProgram)i);
    }
    cout << "Show student with invalid emails" << std::endl;
    classRoster.printInvalidEmails();
    cout << std::endl;

    cout << "Show average days in course" << std::endl;
    for (int i = 0; i < numStudents; i++)
    {
        classRoster.printAverageDaysInCourse(classRoster.classRosterArray[i]->getStudentID());

        return 0;
    }

    cout << "Removing student A3 " << std::endl;
    classRoster.remove("A3");
    cout << std::endl;

    cout << "Removing student A3 " << std::endl;
    classRoster.remove("A3");
    cout << std::endl;

    system("pause");
    return 0;
}
